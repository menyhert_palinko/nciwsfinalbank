# NCIWSFinalBank

Java 8 (only, jaxb is removed from later versions of java)
command to run: mvn jetty:run -f pom.xml

RestFul design and fully object oriented way of handling the requests from the prototype angular application.

Front-end: Angular (typescript)
For front-end development you need the following:
    - Nodejs installed
    - AngularCLI (npm install -g @angular/cli)
    - copyfiles (npm install copyfiles)
    - rimraf (npm install rimraf)
    - mkdirp (npm install mkdirp)
 
 To build the front-end angular app: npm run build
 Build will clean up the webapp folder and copy the bundled files back (this configuration is slightly customized)

The front end app optimized with angular router so it does not reload on every click.

