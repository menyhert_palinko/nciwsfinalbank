package com.ncibankapp.resources;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.get;
import static org.junit.Assert.assertEquals;

/**
 * Customer Resource Test
 *
 * This is not a unit test with mocking, this test does reach out to the API
 * and tests the real results.
 */
public class CustomerResourceTest {

    @Before
    public void setUp(){
        RestAssured.baseURI = "http://127.0.0.1:49000";
        RestAssured.basePath = "/api";
    }

    @Test
    public void testGetCustomer() {

        Response res = get("/customers/1");
        assertEquals(200, res.getStatusCode());
        String json = res.asString();
        JsonPath jp = new JsonPath(json);
        assertEquals("Sherlock Holmes", jp.get("name"));
        assertEquals("221B Baker Street, London", jp.get("address"));
        assertEquals("sherlock@example.com", jp.get("email"));
        assertEquals("1", "" + jp.get("id"));
    }

    @Test
    public void testStatusNotFound() {
        expect().statusCode(404).when().get("/customer/-99");
    }
}
