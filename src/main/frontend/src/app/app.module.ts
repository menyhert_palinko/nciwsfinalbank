import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import { SaveCustomerComponent } from './customer/save-customer/save-customer.component';
import {FormsModule} from "@angular/forms";
import { AccountListComponent } from './account/account-list/account-list.component';
import { SaveAccountComponent } from './account/save-account/save-account.component';
import { TransactionListComponent } from './transaction/transaction-list/transaction-list.component';
import { CreateTransactionComponent } from './transaction/create-transaction/create-transaction.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CustomerListComponent,
    HomeComponent,
    PageNotFoundComponent,
    SaveCustomerComponent,
    AccountListComponent,
    SaveAccountComponent,
    TransactionListComponent,
    CreateTransactionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
