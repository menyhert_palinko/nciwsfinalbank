import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";
import {Transaction} from "./transaction.model";

@Injectable()
export class TransactionService {

  private httpClient: HttpClient;
  private static transactionsAPIUrl:string = "http://127.0.0.1:49000/api/transactions";

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  public listTransactions(): Observable<Transaction[]> {
    return this.httpClient.get<Transaction[]>(TransactionService.transactionsAPIUrl);
  }

  public createTransaction(transaction:Transaction): Observable<Transaction> {
    return this.httpClient.post<Transaction>(TransactionService.transactionsAPIUrl, transaction);
  }
}
