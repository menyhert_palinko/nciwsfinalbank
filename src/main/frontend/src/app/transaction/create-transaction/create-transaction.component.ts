import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TransactionService} from "../transaction.service";
import {Transaction} from "../transaction.model";
import {BankAccount} from "../../account/account.model";

@Component({
  selector: 'app-create-transaction',
  templateUrl: './create-transaction.component.html',
  styleUrls: ['./create-transaction.component.css'],
  providers: [TransactionService]
})
export class CreateTransactionComponent implements OnInit {

  @Output() transactionSaved: EventEmitter<boolean> = new EventEmitter();
  private transactionService: TransactionService;
  public accountId: number;
  public type: string;
  public description: string;
  public amount: number;

  constructor(transactionService: TransactionService) {
    this.transactionService = transactionService;
  }

  ngOnInit() {
  }

  submitCreateTransaction() {

    this.transactionService.createTransaction(new Transaction(0, this.type, this.description, this.amount, "", new BankAccount(this.accountId, "", 0, "", null)))
      .subscribe(transaction => {
        console.log(transaction);
        this.transactionSaved.emit(true);
      });
  }
}
