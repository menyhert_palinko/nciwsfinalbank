import {Component, OnInit} from '@angular/core';
import {TransactionService} from "../transaction.service";
import {Transaction} from "../transaction.model";

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css'],
  providers: [TransactionService]
})
export class TransactionListComponent implements OnInit {

  private transactionService: TransactionService;
  public transactions: Transaction[] = [];
  public isEditingTransaction: boolean = false;
  public showSavedMessage: boolean = false;

  constructor(transactionService: TransactionService) {
    this.transactionService = transactionService;
  }

  ngOnInit() {
    this.listTransactions();
  }

  listTransactions():void {

    this.transactions = [];

    this.transactionService.listTransactions().subscribe(transactions => {
      this.transactions = transactions as Transaction[];
    });
  }

  createTransactionButtonClick():void {
    this.isEditingTransaction = true;
  }

  transactionSavedEvent($event: boolean):void {

    if ($event) {

      this.showMessageAndRefresh();
    }

    this.isEditingTransaction = false;
  }

  private showMessageAndRefresh():void {

    this.showSavedMessage = true;
    this.listTransactions();

    //hide after 3,5 seconds
    setTimeout(function() {
      this.showSavedMessage = false;
    }.bind(this), 3500);
  }

}
