import {BankAccount} from "../account/account.model";

export class Transaction {

  public id: number;
  public type: string;
  public description: string;
  public amount: number;
  public transactionReference: string;
  public account: BankAccount;

  constructor(id: number, type: string, description: string, amount: number, transactionReference: string, account: BankAccount) {
    this.id = id;
    this.type = type;
    this.description = description;
    this.amount = amount;
    this.transactionReference = transactionReference;
    this.account = account;
  }
}
