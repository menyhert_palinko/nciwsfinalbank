import { Component, OnInit } from '@angular/core';
import {Customer} from "../customer.model";
import {CustomerService} from "../customer.service";

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css'],
  providers: [CustomerService]
})
export class CustomerListComponent implements OnInit {

  public customers: Customer[] = [];
  public isEditingCustomer: boolean = false;
  public showSavedMessage: boolean = false;
  public selectedCustomer: Customer = null;

  private customerService: CustomerService;

  constructor(customerService: CustomerService) {
    this.customerService = customerService;
  }

  ngOnInit() {
    this.listCustomers();
  }

  listCustomers():void {

    this.customers = [];

    this.customerService.listCustomers().subscribe(customers => {
      this.customers = customers as Customer[];
    });
  }

  createCustomerButtonClick():void {
    this.selectedCustomer = null;
    this.isEditingCustomer = true;
  }

  customerSavedEvent($event: boolean):void {

    if ($event) {

      this.showMessageAndRefresh();
    }

    this.isEditingCustomer = false;
    this.selectedCustomer = null;
  }

  editCustomer(customer: Customer):void {
    this.selectedCustomer = customer;
    this.isEditingCustomer = true;
  }

  deleteCustomer(customer: Customer):void {
    this.customerService.deleteCustomer(customer).subscribe(customer => {
      console.log(customer);
      this.showMessageAndRefresh();
    });
  }

  private showMessageAndRefresh():void {

    this.showSavedMessage = true;
    this.listCustomers();

    //hide after 3,5 seconds
    setTimeout(function() {
      this.showSavedMessage = false;
    }.bind(this), 3500);
  }
}
