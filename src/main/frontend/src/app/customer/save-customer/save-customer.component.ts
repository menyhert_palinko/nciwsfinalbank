import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CustomerService} from "../customer.service";
import {Customer} from "../customer.model";

@Component({
  selector: 'app-save-customer',
  templateUrl: './save-customer.component.html',
  styleUrls: ['./save-customer.component.css'],
  providers: [CustomerService]
})
export class SaveCustomerComponent implements OnInit {

  @Output() customerSaved: EventEmitter<boolean> = new EventEmitter();
  @Input() selectedCustomer: Customer;
  private customerService: CustomerService;
  public customerName: string;
  public email: string;
  public address: string;

  constructor(customerService: CustomerService) {
    this.customerService = customerService;
  }

  ngOnInit() {

    if (this.selectedCustomer != null) {

      this.customerName = this.selectedCustomer.name;
      this.email = this.selectedCustomer.email;
      this.address = this.selectedCustomer.address;
    }
  }

  submitSaveCustomer() {

    if (this.selectedCustomer == null) {

      //create new customer
      this.customerService.createCustomer(new Customer(0, this.customerName, this.email, this.address))
        .subscribe(customer => {
          console.log(customer);
          this.customerSaved.emit(true);
        });

    } else {

      //update existing customer
      this.customerService.updateCustomer(new Customer(this.selectedCustomer.id, this.customerName, this.email, this.address))
        .subscribe(customer => {
          console.log(customer);
          this.customerSaved.emit(true);
        });

    }
  }
}
