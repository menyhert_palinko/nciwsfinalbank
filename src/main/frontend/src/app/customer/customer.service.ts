import { Injectable } from '@angular/core';
import {Customer} from "./customer.model";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CustomerService {

  private httpClient: HttpClient;
  private static customersAPIUrl:string = "http://127.0.0.1:49000/api/customers";

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  public listCustomers(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(CustomerService.customersAPIUrl);
  }

  public createCustomer(customer:Customer): Observable<Customer> {
    return this.httpClient.post<Customer>(CustomerService.customersAPIUrl, customer);
  }

  public updateCustomer(customer:Customer): Observable<Customer> {
    return this.httpClient.put<Customer>(CustomerService.customersAPIUrl, customer);
  }

  public deleteCustomer(customer:Customer): Observable<Customer> {
    return this.httpClient.delete<Customer>(CustomerService.customersAPIUrl + "/" + customer.id);
  }

}
