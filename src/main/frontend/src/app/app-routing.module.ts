import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomeComponent} from "./home/home.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {CustomerListComponent} from "./customer/customer-list/customer-list.component";
import {RouterModule, Routes} from "@angular/router";
import {AccountListComponent} from "./account/account-list/account-list.component";
import {TransactionListComponent} from "./transaction/transaction-list/transaction-list.component";

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'customers', component: CustomerListComponent },
  { path: 'accounts', component: AccountListComponent },
  { path: 'transactions', component: TransactionListComponent },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/not-found', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [],
  exports: [ RouterModule ]
})
export class AppRoutingModule {

}
