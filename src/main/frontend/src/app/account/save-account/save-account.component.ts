import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AccountService} from "../account.service";
import {BankAccount} from "../account.model";
import {Customer} from "../../customer/customer.model";

@Component({
  selector: 'app-save-account',
  templateUrl: './save-account.component.html',
  styleUrls: ['./save-account.component.css'],
  providers: [AccountService]
})
export class SaveAccountComponent implements OnInit {

  private accountService: AccountService;
  @Input() public selectedAccount: BankAccount;
  @Output() accountSaved: EventEmitter<boolean> =   new EventEmitter();
  public accountNumber: string;
  public balance: number;
  public shortCode: string;
  public customerId: number;

  constructor(accountService: AccountService) {
    this.accountService = accountService;
  }

  ngOnInit() {

    if (this.selectedAccount != null) {

      this.accountNumber = this.selectedAccount.accountNumber;
      this.balance = this.selectedAccount.balance;
      this.shortCode = this.selectedAccount.shortCode;
    }
  }

  submitSaveAccount() {

    if (this.selectedAccount == null) {

      //create new account
      this.accountService.createAccount(new BankAccount(0, this.accountNumber, this.balance, this.shortCode, new Customer(this.customerId, "", "", "")))
        .subscribe(account => {
          console.log(account);
          this.accountSaved.emit(true);
        });

    } else {

      //update existing account
      this.accountService.updateAccount(new BankAccount(this.selectedAccount.id, this.accountNumber, this.balance, this.shortCode, this.selectedAccount.customer))
        .subscribe(account => {
          console.log(account);
          this.accountSaved.emit(true);
        });

    }
  }
}
