import {Customer} from "../customer/customer.model";

export class BankAccount {

  public id: number;
  public accountNumber: string;
  public balance: number;
  public shortCode: string;
  public customer: Customer;

  constructor(id: number, accountNumber: string, balance: number, shortCode: string, customer: Customer) {
    this.id = id;
    this.accountNumber = accountNumber;
    this.balance = balance;
    this.shortCode = shortCode;
    this.customer = customer;
  }
}
