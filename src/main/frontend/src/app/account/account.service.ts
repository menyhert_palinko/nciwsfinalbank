import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BankAccount} from "./account.model";

@Injectable()
export class AccountService {

  private httpClient: HttpClient;
  private static accountsAPIUrl:string = "http://127.0.0.1:49000/api/accounts";

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  public listAccounts(): Observable<BankAccount[]> {
    return this.httpClient.get<BankAccount[]>(AccountService.accountsAPIUrl);
  }

  createAccount(account: BankAccount): Observable<BankAccount> {
    return this.httpClient.post<BankAccount>(AccountService.accountsAPIUrl, account);
  }

  updateAccount(account: BankAccount): Observable<BankAccount>  {
    return this.httpClient.put<BankAccount>(AccountService.accountsAPIUrl, account);
  }

  public deleteAccount(account: BankAccount): Observable<BankAccount> {
    return this.httpClient.delete<BankAccount>(AccountService.accountsAPIUrl + "/" + account.id);
  }
}
