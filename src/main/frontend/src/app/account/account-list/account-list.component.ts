import { Component, OnInit } from '@angular/core';
import {BankAccount} from "../account.model";
import {AccountService} from "../account.service";

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css'],
  providers: [AccountService]
})
export class AccountListComponent implements OnInit {

  private accountService: AccountService;
  public accounts: BankAccount[] = [];
  public isEditingAccount: boolean = false;
  public showSavedMessage: boolean = false;
  public selectedAccount: BankAccount = null;

  constructor(accountService: AccountService) {
    this.accountService = accountService;
  }

  ngOnInit() {
    this.listAccounts();
  }

  listAccounts(): void {

    this.accounts = [];

    this.accountService.listAccounts().subscribe(accounts => {
      this.accounts = accounts as BankAccount[];
    });
  }

  createAccountButtonClick():void {
    this.selectedAccount = null;
    this.isEditingAccount = true;
  }

  accountSavedEvent($event: boolean):void {

    if ($event) {
      this.showMessageAndRefresh();
    }

    this.isEditingAccount = false;
    this.selectedAccount = null;
  }

  editAccount(account: BankAccount) {
    this.selectedAccount = account;
    this.isEditingAccount = true;
  }

  deleteAccount(account: BankAccount) {
    this.accountService.deleteAccount(account).subscribe(account => {
      console.log(account);
      this.showMessageAndRefresh();
    });
  }

  private showMessageAndRefresh():void {

    this.showSavedMessage = true;
    this.listAccounts();

    //hide after 3,5 seconds
    setTimeout(function() {
      this.showSavedMessage = false;
    }.bind(this), 3500);
  }
}
