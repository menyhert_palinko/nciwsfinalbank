package com.ncibankapp.resources;

import com.ncibankapp.common.ServerConstants;
import com.ncibankapp.entities.Account;
import com.ncibankapp.model.ErrorMessage;
import com.ncibankapp.service.AccountService;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

@Path("/accounts")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class AccountResource {

    private static final AccountService accountService = new AccountService();

    @Context
    private UriInfo uriInfo;

    @GET
    public Response getAccounts() {


        try {

            GenericEntity<List<Account>> list = new GenericEntity<List<Account>>(accountService.listAccounts()) {};
            return Response.status(Response.Status.OK).entity(list).build();

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getAccount(@PathParam("id") long id) {

        Account account = null;

        try {

            account = accountService.getAccountById(id);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }

        return Response.status(Response.Status.OK).entity(account).build();
    }

    @POST
    public Response createAccount(Account account) {

        try {

            account = accountService.createAccount(account);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }

        URI uri = uriInfo.getAbsolutePathBuilder().path(account.getId() + "").build();
        return Response.created(uri).entity(account).type(MediaType.APPLICATION_JSON).build();
    }

    @PUT
    public Response updateAccount(Account account) {

        try {

            account = accountService.updateAccount(account);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
        return Response.status(Response.Status.OK).entity(account).build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteAccount(@PathParam("id") long id) {

        Account account = null;

        try {

            account = accountService.deleteAccount(id);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }

        return Response.status(Response.Status.OK).entity(account).build();
    }
}
