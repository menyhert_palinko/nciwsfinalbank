package com.ncibankapp.resources;

import com.ncibankapp.common.ServerConstants;
import com.ncibankapp.entities.Account;
import com.ncibankapp.entities.Customer;
import com.ncibankapp.model.ErrorMessage;
import com.ncibankapp.service.CustomerService;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.Collections;
import java.util.List;

@Path("/customers")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class CustomerResource {

    @Context
    private UriInfo uriInfo;

    private static final CustomerService customerService = CustomerService.INSTANCE;

    @GET
    public Response getCustomers() {

        try {

            GenericEntity<List<Customer>> list = new GenericEntity<List<Customer>>(customerService.listCustomers()) {};
            return Response.status(Response.Status.OK).entity(list).build();

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getCustomer(@PathParam("id") long id) {

        Customer customer = null;
        try {

            customer = customerService.getCustomerById(id);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
        return Response.status(Response.Status.OK).entity(customer).build();
    }

    @POST
    public Response createCustomer(Customer customer) {

        try {

            customer = customerService.createCustomer(customer);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }

        URI uri = uriInfo.getAbsolutePathBuilder().path(customer.getId() + "").build();
        return Response.created(uri).entity(customer).build();
    }
    
    @PUT
    public Response updateCustomer(Customer customer) {

        try {

            customer = customerService.updateCustomer(customer);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
        return Response.status(Response.Status.OK).entity(customer).build();
    }
    
    @DELETE
    @Path("{id}")
    public Response deleteCustomer(@PathParam("id") long id) {

        Customer customer = null;
        try {

            customer = customerService.deleteCustomer(id);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
        return Response.status(Response.Status.OK).entity(customer).build();
    }

    @GET
    @Path("/{customerId}/accounts")
    public Response listUserAddresses(@PathParam("customerId") long customerId) {

        try {

            GenericEntity<List<Account>> list = new GenericEntity<List<Account>>(customerService.listCustomerAccounts(customerId)) {};
            return Response.status(Response.Status.OK)
                    .entity(list).build();

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
    }

    @GET
    @Path("/find")
    public Response searchForCustomers(@QueryParam("name") String name, @QueryParam("email") String email) {

        try {

            GenericEntity<List<Customer>> list = new GenericEntity<List<Customer>>(customerService.search(name, email)) {};
            return Response.status(Response.Status.OK)
                    .entity(list).build();

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
    }

}
