package com.ncibankapp.resources;

import com.ncibankapp.common.ServerConstants;
import com.ncibankapp.entities.Transaction;
import com.ncibankapp.model.ErrorMessage;
import com.ncibankapp.service.TransactionService;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

@Path("/transactions")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class TransactionResource {

    @Context
    private UriInfo uriInfo;

    private static final TransactionService transactionService = new TransactionService();

    @GET
    public Response listTransactions() {

        try {

            GenericEntity<List<Transaction>> list = new GenericEntity<List<Transaction>>(transactionService.listTransactions()) {};
            return Response.status(Response.Status.OK).entity(list).build();

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getTransaction(@PathParam("id") long id) {

        Transaction transaction = null;
        try {

            transaction = transactionService.getTransactionById(id);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }
        return Response.status(Response.Status.OK).entity(transaction).build();
    }

    @POST
    public Response createTransaction(Transaction transaction) {

        try {

            transaction = transactionService.createTransaction(transaction);

        } catch (Exception e) {

            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorMessage(ServerConstants.API_FAILURE_CODE, ServerConstants.API_FAILURE_TEXT))
                    .build();
        }

        URI uri = uriInfo.getAbsolutePathBuilder().path(transaction.getId() + "").build();
        return Response.created(uri).entity(transaction).build();
    }
}
