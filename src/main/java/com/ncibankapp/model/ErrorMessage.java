package com.ncibankapp.model;

/**
 * POJO designed to be serialised to JSON
 * returned in case of problems
 */
public class ErrorMessage extends ResponseMessage {

    public ErrorMessage() {
    }

    public ErrorMessage(int code, String message) {
        super(code, message);
    }
}
