package com.ncibankapp.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResponseMessage {

    /**
     * One of the Best practices to have a meaningful code
     * Usually internally standardized within an organization
     */
    private int code;

    /**
     * Error message to show the user what the problem is
     */
    private String message;

    ResponseMessage() {
    }

    ResponseMessage(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
