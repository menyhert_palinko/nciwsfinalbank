package com.ncibankapp.common;

public class ServerConstants {

    public static final String PERSISTENT_UNIT_NAME = "ncibankapp";
    public static final int API_FAILURE_CODE = 1100;
    public static final String API_FAILURE_TEXT = "API Failure";

}
