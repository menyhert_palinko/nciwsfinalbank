package com.ncibankapp.common;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ServerEntityManagerFactory {

    private static final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(ServerConstants.PERSISTENT_UNIT_NAME);

    public static EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }
}
