package com.ncibankapp.service;

import com.ncibankapp.common.ServerEntityManagerFactory;
import com.ncibankapp.entities.Account;
import com.ncibankapp.entities.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.UUID;

public class TransactionService {

    private EntityManagerFactory entityManagerFactory = ServerEntityManagerFactory.getEntityManagerFactory();

    public List<Transaction> listTransactions() throws Exception {

        EntityManager entityManager = getEntityManager();
        /**
         * As we use hibernate, here instead of JPQL syntax we can use HQL syntax.
         */
        List<Transaction> transactions = entityManager.createQuery("FROM Transaction", Transaction.class).getResultList();
        entityManager.close();
        return transactions;
    }

    public Transaction getTransactionById(long id) throws Exception {

        EntityManager entityManager = getEntityManager();
        Transaction transaction = entityManager.find(Transaction.class, id);
        entityManager.close();
        if (transaction == null) throw new Exception("Transaction not found by id: " + id);
        return transaction;
    }

    public Transaction createTransaction(Transaction transaction) throws Exception {

        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();

        //to make sure to don't persist transaction with any invalid account
        Account account = entityManager.find(Account.class, transaction.getAccount().getId());
        if (account == null) throw new Exception("Invalid Account. id: " + transaction.getAccount().getId());

        transaction.setAccount(account);

        /*
        Amount is updated here, however, in real world project it would be batch processing
        Probably after plenty of checks and approves.
         */

        if (transaction.getType().equalsIgnoreCase("CREDIT")) {

            //balance check
            if (transaction.getAmount() > account.getBalance())
                throw new Exception("Insufficient balance. AccountID: " + account.getId());

            //substract the amount from the balance
            account.setBalance(account.getBalance() - transaction.getAmount());

        } else {

            //debit
            //add the amount to the balance
            account.setBalance(account.getBalance() + transaction.getAmount());
        }

        entityManager.merge(account);

        //server generated the transaction reference
        transaction.setTransactionReference(UUID.randomUUID().toString());
        entityManager.persist(transaction);

        entityManager.getTransaction().commit();
        entityManager.close();
        return transaction;
    }

    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
