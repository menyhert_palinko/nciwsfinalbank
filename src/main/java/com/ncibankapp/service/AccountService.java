package com.ncibankapp.service;

import com.ncibankapp.common.ServerEntityManagerFactory;
import com.ncibankapp.entities.Account;
import com.ncibankapp.entities.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

/**
 * As this project is around API we do not have EJB module for the business logic.
 * Therefore Services classes will implement operations with the persistent layer.
 */
public class AccountService {

    private static final CustomerService customerService = CustomerService.INSTANCE;
    private EntityManagerFactory entityManagerFactory = ServerEntityManagerFactory.getEntityManagerFactory();

    public AccountService() {
    }

    public List<Account> listAccounts() throws Exception {

        EntityManager entityManager = getEntityManager();
        /**
         * As we use hibernate, here instead of JPQL syntax we can use HQL syntax.
         */
        List<Account> accounts = entityManager.createQuery("FROM Account", Account.class).getResultList();

        entityManager.close();
        return accounts;
    }

    public Account getAccountById(long id) throws Exception {

        EntityManager entityManager = getEntityManager();
        Account account = entityManager.find(Account.class, id);
        entityManager.close();

        if (account == null) throw new Exception("Account not found by id: " + id);

        return account;
    }

    public Account createAccount(Account account) throws Exception {

        //get reference for the customer who's the owner of the account
        Customer customer = customerService.getCustomerById(account.getCustomer().getId());
        account.setCustomer(customer);

        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(account);
        entityManager.getTransaction().commit();
        entityManager.close();
        return account;
    }

    public Account updateAccount(Account account) throws Exception {

        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();

        Account accountToSave = entityManager.find(Account.class, account.getId());

        if (accountToSave == null) throw new Exception("Account not found by id: " + account.getId());

        //it is prohibited to change the owner of the account.
        //update only the details are allowed to update
        accountToSave.setAccountNumber(account.getAccountNumber());
        accountToSave.setBalance(account.getBalance());
        accountToSave.setShortCode(account.getShortCode());
        entityManager.merge(accountToSave);

        entityManager.getTransaction().commit();
        entityManager.close();
        return accountToSave;
    }

    public Account deleteAccount(long id) throws Exception {

        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();

        Account accountToDelete = entityManager.find(Account.class, id);

        if (accountToDelete == null) throw new Exception("Account not found by id: " + id);

        entityManager.remove(accountToDelete);
        entityManager.getTransaction().commit();
        entityManager.close();
        return accountToDelete;
    }

    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
