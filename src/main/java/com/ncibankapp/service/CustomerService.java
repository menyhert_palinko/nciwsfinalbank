package com.ncibankapp.service;

import com.ncibankapp.common.ServerEntityManagerFactory;
import com.ncibankapp.entities.Account;
import com.ncibankapp.entities.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.Collections;
import java.util.List;

/**
 * As this project is around API we do not have EJB module for the business logic.
 * Therefore Services classes will implement operations with the persistent layer.
 */
public class CustomerService {

    public static final CustomerService INSTANCE = new CustomerService();

    private EntityManagerFactory entityManagerFactory = ServerEntityManagerFactory.getEntityManagerFactory();

    private CustomerService() {

        /*
        * Create some default data as we are working with inMemoryDB
        * It is useful fot running the tests in the test package. Some
        * of them will hit the real api instead of mocking.
         */
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();

        Customer customer = new Customer();
        customer.setName("Sherlock Holmes");
        customer.setEmail("sherlock@example.com");
        customer.setAddress("221B Baker Street, London");
        entityManager.persist(customer);

        Customer customer2 = new Customer();
        customer2.setName("John Watson");
        customer2.setEmail("john.watson@example.com");
        customer2.setAddress("221B Baker Street, London");

        entityManager.persist(customer2);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public List<Customer> listCustomers() throws Exception {

        EntityManager entityManager = getEntityManager();
        /**
         * As we use hibernate, here instead of JPQL syntax we can use HQL syntax.
         */
        List<Customer> customers = entityManager.createQuery("FROM Customer", Customer.class).getResultList();
        entityManager.close();
        return customers;
    }

    public Customer getCustomerById(long id) throws Exception {

        EntityManager entityManager = getEntityManager();
        Customer customer = entityManager.find(Customer.class, id);
        entityManager.close();
        if (customer == null) throw new Exception("Customer is not found by id: " + id);
        return customer;
    }

    public Customer createCustomer(Customer customer) throws Exception {

        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(customer);
        entityManager.getTransaction().commit();
        entityManager.close();
        return customer;
    }
    
    public Customer updateCustomer(Customer customer) throws Exception {

        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(customer);
        entityManager.getTransaction().commit();
        entityManager.close();
        return customer;
    }
    
    public Customer deleteCustomer(long id) throws Exception {

        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        
        Customer customerToDelete = entityManager.find(Customer.class, id);
        if (customerToDelete == null) throw new Exception("Customer is not found by id: " + id);

        entityManager.remove(customerToDelete);
        entityManager.getTransaction().commit();
        entityManager.close();
        return customerToDelete;
    }

    public List<Customer> search(String name, String email) throws Exception {

        boolean nameSearch = (name != null && !name.equals(""));
        boolean emailSearch = (email != null && !email.equals(""));

        if (!nameSearch && !emailSearch) {
            //no search
            return listCustomers();
        }

        EntityManager entityManager = getEntityManager();

        StringBuilder sb = new StringBuilder("SELECT customer FROM Customer customer WHERE ");
        boolean and = false;

        if (nameSearch) {
            sb.append("customer.name LIKE :name");
        }

        if (emailSearch) {
            if (and) sb.append(" AND ");
            sb.append("customer.email LIKE :email");
        }

        Query query = entityManager.createQuery("FROM Customer", Customer.class);

        if (nameSearch) {
            query.setParameter("name", name);
        }

        if (emailSearch) {
            query.setParameter("email", email);
        }

        List<Customer> customers = query.getResultList();
        entityManager.close();
        return customers;
    }

    public List<Account> listCustomerAccounts(long customerId) throws Exception {

        EntityManager entityManager = getEntityManager();

        Customer customer = entityManager.find(Customer.class, customerId);

        if (customer == null) throw new Exception("Customer is not found by id: " + customerId);

        List<Account> accounts = customer.getAccounts();

        entityManager.close();
        return accounts;
    }

    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
